package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/jehiah/go-strftime"
	"golang.org/x/text/encoding/traditionalchinese"
	"golang.org/x/text/transform"
)

type DataNotFoundErr struct {
	msg string
}

func (e *DataNotFoundErr) Error() string {
	return e.msg
}

func GetTWTime() time.Time {
	t := time.Now()
	localLocation, err := time.LoadLocation("Asia/Taipei")
	if err != nil {
		fmt.Println(err)
	}
	timeUTC := t.In(localLocation)
	return timeUTC
}

func DecodeBig5(s []byte) ([]byte, error) {
	I := bytes.NewReader(s)
	O := transform.NewReader(I, traditionalchinese.Big5.NewDecoder())
	d, e := ioutil.ReadAll(O)
	if e != nil {
		return nil, e
	}
	return d, nil
}

func GetTWStockExchangeCsv(stockType string, date time.Time) ([]byte, error) {
	timeStr := strftime.Format("%Y%m%d", date)
	url := fmt.Sprintf("https://www.twse.com.tw/exchangeReport/MI_INDEX?response=csv&date=%s&type=%s", timeStr, stockType)
	fmt.Printf("Start downloading from %s\n", url)
	res, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	defer time.Sleep(3 * time.Second)

	sitemap, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	if len(sitemap) <= 0 {
		return nil, &DataNotFoundErr{fmt.Sprintf("%s doesn't have data", timeStr)}
	}

	return DecodeBig5(sitemap)
}

func main() {
	var startDateStr = flag.String("start", "", "Start date, e.g 2019-11-1")
	var endDateStr = flag.String("end", "", "End date, e.g 2019-11-2")
	var stockType = flag.String("stock-type", "01", "Read README.md")
	flag.Parse()
	layout := "2006-1-2"
	startDate, err := time.Parse(layout, *startDateStr)
	if err != nil {
		log.Fatal(err)
	}
	endDate, err := time.Parse(layout, *endDateStr)
	if err != nil {
		log.Fatal(err)
	}

	if !startDate.Before(endDate) && !startDate.Equal(endDate) {
		fmt.Println("Start date should be before end date")
		os.Exit(-1)
	}
	for date := startDate; !date.After(endDate); date = date.AddDate(0, 0, 1) {
		timeStr := strftime.Format("%Y%m%d", date)
		csvData, err := GetTWStockExchangeCsv(*stockType, date)
		if err != nil {
			switch err.(type) {
			case *DataNotFoundErr:
				log.Println(err)
				continue
			default:
				log.Fatal(err)
			}

		}
		err = ioutil.WriteFile(fmt.Sprintf("exchange_data_type_%s_%s.csv", *stockType, timeStr), csvData, 0644)
		if err != nil {
			log.Fatal(err)
		}
	}

}
