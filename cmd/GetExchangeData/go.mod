module gitlab.com/uniray7/twstockcrawler

go 1.12

require (
	github.com/jehiah/go-strftime v0.0.0-20171201141054-1d33003b3869
	golang.org/x/text v0.3.2
)
